/* FUNCTIONS */
//Build Listing Cards
function buildListingCards(payload) {
  
  payload.forEach(function(listing) {
        
    // Build the listing stats
    var price = listing.price;
    var baths = listing.baths;
    var beds = listing.beds;
    var sqft = listing.main_floor_area;

    // Build the listing address 
    var unitNum = listing.unit;
    var streetNum = listing.num;
    var streetName = listing.street;
    var streetType = listing.streettype;
    var area = listing.area;
    var city = listing.city;
    var slug = listing.slug;
    var province = 'BC';
    var manualAddressONE = unitNum + '-' + streetNum + ' ' + streetName + ' ' + streetType; 
    var manualAddressTWO = city + ', ' + province;

    // Build the initial listings cards
    $('#listings-container').append('<div class="shortListing-outer-container col-sm-12 col-md-4"> <div class="shortListing-image-price-container row"> <div class="col-sm-12"> <img src="https://s-media-cache-ak0.pinimg.com/736x/73/de/32/73de32f9e5a0db66ec7805bb7cb3f807.jpg" /> <p>$' + price + '</p> </div> </div> <div class="shortListing-stats-container row"> <div class="col-sm-12"> <div class="row"> <div class="col-sm-4"><p>' + beds +'Beds</p></div> <div class="col-sm-4"><p>' + baths + 'Baths</p></div> <div class="col-sm-4"><p>' + sqft + 'sqft</p></div> </div> </div> </div> <div class="shortListing-address-container row"> <div class="col-sm-12"> <div class="row"> <div class="col-sm-12"><p>' + manualAddressONE + '</p></div> <div class="col-sm-12"><p>' + manualAddressTWO + '</p></div> </div> </div> </div> <div class="shortListing-cta-container row"> <div class="col-sm-12"> <a><span class="shortListing-cta-view">VIEW NOW</span></a> </div> </div> </div> ');
  }); // InitialListingsToShow   

}

// SORT Filter Logic
function sortFilterLogic(sort, theData) {
  sortType = sort.attr('class');

  // The sort logic
  if (sortType == 'price-asc') {
    theData = theData.sort(function(a, b) {
      return a.price - b.price;
    });
  } else if (sortType == 'price-desc') {
     theData = theData.sort(function(a, b) {
      return b.price - a.price;
    });
  } else if (sortType == 'sqft-asc') {
    theData = theData.sort(function(a, b) {
      return a.main_floor_area - b.main_floor_area;
    });
  } else if (sortType == 'sqft-desc') {
     theData = theData.sort(function(a, b) {
      return b.main_floor_area - a.main_floor_area;
    });
  } else if (sortType == 'beds-asc') {
    theData = theData.sort(function(a, b) {
      return a.beds - b.beds;
    });
  } else if (sortType == 'beds-desc') {
     theData = theData.sort(function(a, b) {
      return b.beds - a.beds;
    });
  }

  if (sortType == 'price-asc') {
    sortTypeText = 'PRICE ASC';
  } else if (sortType == 'price-desc') {
    sortTypeText =  'PRICE DESC';
  } else if (sortType == 'sqft-asc') {
    sortTypeText =  'SQFT ASC';
  } else if (sortType == 'sqft-desc') {
    sortTypeText =  'SQFT DESC';
  } else if (sortType == 'beds-asc') {
    sortTypeText =  'BEDS ASC';
  } else if (sortType == 'beds-desc') {
    sortTypeText =  'BEDS DESC';
  } else {
    sortTypeText = 'Sort by';
  } 

  if ($('.simple-pagination ul > li:nth-child(2)').hasClass('active')) {
    $('#listings-pagination-top > ul > li:nth-child(3) > a').get(0).click();
    $('#listings-pagination-top > ul > li:nth-child(2) > a').get(0).click();
  } else {
    $('#listings-pagination-top > ul > li:nth-child(2) > a').get(0).click();
  }
}

// PaginationLogic
function paginationLogic(pageNum) {
  var prevNum = pageNum - 1;
  $('#listings-pagination-top .page-link.prev').attr('page', null);
  $('#listings-pagination-top .page-link.prev').attr('page', prevNum);
  $('#listings-pagination-bottom .page-link.prev').attr('page', null);
  $('#listings-pagination-bottom .page-link.prev').attr('page', prevNum);

  var nextNum = pageNum + 1;
  $('#listings-pagination-top .page-link.next').attr('page', null);
  $('#listings-pagination-top .page-link.next').attr('page', nextNum);
  $('#listings-pagination-bottom .page-link.next').attr('page', null);
  $('#listings-pagination-bottom .page-link.next').attr('page', nextNum);

  // When either of the prev or next links are disabled (user has selected either the first or last page), manually set the attr('page')
  if ($('#listings-pagination-bottom > ul > li.disabled > .prev').length) {
    $('#listings-pagination-top .page-link.next').attr('page', null);
    $('#listings-pagination-top .page-link.next').attr('page', 2);
    $('#listings-pagination-bottom .page-link.next').attr('page', null);
    $('#listings-pagination-bottom .page-link.next').attr('page', 2);

  } else if (($('#listings-pagination-bottom > ul > li.disabled > .next').length)) {
    
    var pagesTotal = $('#listings-pagination-bottom li').length - 2;
    var pageBeforeLast = pagesTotal - 1;

    $('#listings-pagination-top .page-link.prev').attr('page', null);
    $('#listings-pagination-top .page-link.prev').attr('page', pageBeforeLast);
    $('#listings-pagination-bottom .page-link.prev').attr('page', null);
    $('#listings-pagination-bottom .page-link.prev').attr('page', pageBeforeLast);
  }
}

/* End of FUNCTIONS */

/// ////////////////////////////////////////// RUN QUERY //////////////////////////////////////////
$.ajax({
  url: "https://8pjgstiks1.execute-api.us-west-2.amazonaws.com/dev/residences/firm/sutton-premier-realty", 
  success: function(result) {
    var theData = result;
    var sortTypeText ;
    
    //[CUSTOMISATION POINT 0] - set number of listings per page
    var listingsPerPage = 6;
    
    // Create pagination Nav using plugin
    $('#listings-pagination-bottom, #listings-pagination-top').pagination({
      items: theData.length,
      itemsOnPage: listingsPerPage,
      cssStyle: 'light-theme'
    });

    $('#listings-pagination-top .page-link.next').attr('page', '2');
    $('#listings-pagination-bottom .page-link.next').attr('page', '2');

    // Add sort filter
    $('#listings-pagination-top').prepend('<div class="dropdown shortCard-sort-filter"> <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> Sort by<span class="caret"></span> </button> <ul class="dropdown-menu"> <li class="price-asc"><a>Price ASC</a></li> <li class="price-desc"><a>Price DESC</a></li> <li class="sqft-asc"><a>Sqft ASC</a></li> <li class="sqft-desc"><a>Sqft DESC</a></li> <li class="beds-asc"><a>Beds ASC</a></li> <li class="beds-desc"><a>Beds DESC</a></li> </ul> </div>');

    // Display initial listings on page load
    var initialListingsToShow = [];

    for (i=0; i <= 5; i++) {
      if (typeof theData[i] != 'undefined') {
        initialListingsToShow.push(theData[i]);
      }
    }

    buildListingCards(initialListingsToShow);

    // [CUSTOMISATION POINT 2] -  select specific element identifiers that will need go be adjusted
    // !!!!!!! Move the elements further down due to Margin-top of #listings-pagination-top (see below for these elements being positioned)
    var initialElementsToMove = ['#lp-pom-image-805', '#lp-pom-text-68', '#lp-pom-text-69', '#lp-pom-box-91', '#lp-pom-image-93', '#lp-pom-text-94', '#lp-pom-text-95', '#lp-pom-box-75', '#lp-pom-image-985', '#lp-pom-text-72', '#lp-pom-text-73', '#lp-pom-box-97', '#lp-pom-image-99', '#lp-pom-text-100', '#lp-pom-text-101', '#lp-pom-text-408', '#lp-pom-text-409', '#lp-pom-form-389', '#lp-pom-text-649', '#lp-pom-text-146', '#lp-pom-button-766', '#lp-pom-image-960'];
    
    initialElementsToMove.forEach(function(ele) {
      var top = $(ele).css('top');
      var newTop = parseInt(top);
      newTop = newTop + 60; // this value is the margin-top value of #listings-pagination-top
      $(ele).css('top', newTop);
    }); 

    // Everytime someone clicks one of the pagination navs, SELECT all queried listings associated with page, and build cards
    $('#listings-pagination-bottom, #listings-pagination-top').on('click', '.page-link, .current', function() {

      if (typeof sortTypeText == 'undefined') {
        sortTypeText = 'Sort by';
      }
      // For some reason when you click a pagination nav, it deletes the sortBy dropdown too, so append it every time 
      $('#listings-pagination-top').prepend('<div class="dropdown shortCard-sort-filter"> <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">' + sortTypeText + '<span class="caret"></span> </button> <ul class="dropdown-menu"> <li class="price-asc"><a>Price ASC</a></li> <li class="price-desc"><a>Price DESC</a></li> <li class="sqft-asc"><a>Sqft ASC</a></li> <li class="sqft-desc"><a>Sqft DESC</a></li> <li class="beds-asc"><a>Beds ASC</a></li> <li class="beds-desc"><a>Beds DESC</a></li> </ul> </div>');

      location.hash = '#listings-container';

      // Removed default href behaviour from plugin file because it had to potential to keep deleting the search query params whenever someone selected a new page
      // Clicking prev/next will grab the attr('page'), which is called on page load, and when a user clicks a page link button
      var pageNum;

      if ($(this).text() == 'Prev' || $(this).text() == 'Next') {
        pageNum = $(this).attr('page');
        pageNum = parseInt(pageNum);
      } else {
        pageNum = $(this).text();
        pageNum = parseInt(pageNum);
      }

      paginationLogic(pageNum);

      // [CUSTOMISATION POINT 3] - listingsperPage determined at the very top 
      // Set the range of listings to show, based on index
      var lowerBound = (listingsPerPage * pageNum ) - listingsPerPage;
      var upperBound = (listingsPerPage * pageNum ) - 1;

      var listingsToShow = [];

      for (i=lowerBound; i <= upperBound; i++) {
        if (typeof theData[i] != 'undefined') {
          listingsToShow.push(theData[i]);
        }
      }

      // Remove all current listings **** SHOULD SHOW LOADING SPINNER for ~1 sec to let users know data has changed ****
      $('#listings-container').empty();

      // Build variables for each listing
      buildListingCards(listingsToShow);    

      // [CUSTOMISATION POINT 4]
      /* Migrate elements up/down the page depending on how many rows of cards there are on dynamic listings section of the page */
      // 1. Assess number of cards on page
      var numCards = $('.shortListing-outer-container');
      var numRows;
      var rowHeight = 300; // CUSTOMIZE based on row-height which will depend on height of cards

      if (numCards.length >= 4) {
        numRows = 2;
      } else {
        numRows = 1;
      } 
     
      var listingContainerHeight = $('#listings-container').css('height');
      listingContainerHeight = parseInt(listingContainerHeight);

      var pixelChange;

      // [CUSTOMISATION POINT 5] - listingContainerHeight == ? equivalence will depend on rowheight
      // 2. Logic to determine how much each element/section needs to flex by depending on the state
      if ( (listingContainerHeight == 638 && numRows == 2) || (listingContainerHeight == 338 && numRows == 1) ) {

        pixelChange = 0;

      } else if ( listingContainerHeight == 638 && numRows == 1 ) {

        pixelChange = -rowHeight;
        $('#listings-container').css('height', listingContainerHeight - rowHeight);

      } else if ( listingContainerHeight == 338 && numRows == 2) {

        pixelChange = rowHeight;
        $('#listings-container').css('height', listingContainerHeight + rowHeight);

      } 

      // [CUSTOMISATION POINT 6]
      // Create array of elements below the dynamic section that need to migrate
      var elementsToMove = ['#lp-pom-image-805', '#lp-pom-text-68', '#lp-pom-text-69', '#lp-pom-box-91', '#lp-pom-image-93', '#lp-pom-text-94', '#lp-pom-text-95', '#lp-pom-box-75', '#lp-pom-image-985', '#lp-pom-text-72', '#lp-pom-text-73', '#lp-pom-box-97', '#lp-pom-image-99', '#lp-pom-text-100', '#lp-pom-text-101', '#lp-pom-text-408', '#lp-pom-text-409', '#lp-pom-form-389', '#lp-pom-text-649', '#lp-pom-text-146', '#lp-pom-button-766', '#lp-pom-image-960'];

      elementsToMove.forEach(function(ele) {
        var top = $(ele).css('top');
        var newTop = parseInt(top);
        newTop = newTop + pixelChange;
        $(ele).css('top', newTop);
      });

       // Create array of sections that need to have their height changed
      var sectionsToMove = ['#lp-pom-root', '#lp-pom-block-543'];

      sectionsToMove.forEach(function(sec) {
        var height = $(sec).css('height');
        var newHeight = parseInt(height);
        newHeight = newHeight + pixelChange;
        $(sec).css('height', newHeight);
      });

      $('#lp-pom-root-color-overlay').css('height', $('#lp-pom-root').css('height'));
 
    }); // listings-pagination onClick
    
    // Sort Filter logic
    // ASCENDING: a.price - b.price 
    $('#listings-pagination-bottom, #listings-pagination-top').on('click', '.shortCard-sort-filter li', function() {
      sortFilterLogic($(this), theData);
    }); // sortFilter
  } // success         
}); //ajax
